import QtQuick 2.11
import QtQuick.Controls 2.4
import QtGraphicalEffects 1.12

Page {
//    anchors.fill: parent
    title: qsTr("Page 2")
    Label {
        text: qsTr("You are on Page 2.")
        anchors.centerIn: parent
    }

    Rectangle {
        anchors.fill: parent
        color: "green"
    }

    Item {
        anchors.fill: parent

        Image {
            id: bug
            source: "../resources/bug.png"
            sourceSize: Qt.size(parent.width, parent.height)
            smooth: true
            visible: false
            width: parent.width * 0.5
            height: parent.width * 0.5
            opacity: 0.5
        }

//        Image {
//            id: butterfly
//            source: "../resources/butterfly.png"
//            sourceSize: Qt.size(parent.width, parent.height)
//            smooth: true
//            visible: false
//        }

        Rectangle {
            id: rectMask
            width: bug.width
            height: bug.height
            smooth: true
            anchors.centerIn: parent
            visible: false
            radius: Math.max(width/2, height/2)
            color: "#00000000"
            LinearGradient {
                anchors.fill: parent
                start: Qt.point(0, 0)
                end: Qt.point(0, bug.height * 0.5)
                gradient: Gradient {
                    GradientStop { position: 0.0; color: "#FFFFFFFF" }
                    GradientStop { position: 1.0; color: "#00000000" }
                }
            }
        }


        OpacityMask {
            id: mask
//            invert: true
            width: bug.width
            height: bug.height
            source: bug
            maskSource: rectMask
        }

//        Rectangle {
//            width: 200
//            height: 100
//            gradient: Gradient {
//                GradientStop { position: 0; color: "white" }
//                GradientStop { position: 1; color: "black" }
//            }
//            Row {
//                opacity: 1
//                Item {
//                    id: foo
//                    width: 100; height: 100
//                    Rectangle {
//                        x: 5; y: 5; width: 60; height: 60; color: "red"
//                    }
//                    Rectangle {
//                        x: 20; y: 20; width: 60; height: 60; color: "#00000000"
//                    }
//                    Rectangle {
//                        x: 35; y: 35; width: 60; height: 60; color: "yellow"
//                        LinearGradient {
//                            anchors.fill: parent
//                            start: Qt.point(0, 0)
//                            end: Qt.point(0, 50)
//                            gradient: Gradient {
//                                GradientStop { position: 0.0; color: "#00FFFFFF" }
//                                GradientStop { position: 1.0; color: "#FFFFFFFF" }
//                            }
//                        }
//                    }
//                }
//                ShaderEffectSource {
//                    width: 100; height: 100
//                    sourceItem: foo
//                }
//            }
//        }
//        Blend {
//            anchors.fill: bug
//            source: bug
//            foregroundSource: rectMask
//            mode: "softLight"
//        }
    }
}

//curl "http://mobileapi.fsrar.ru/api/mark?DataMatrix=228-06774519126220517312268008442&PDF417=22N00002NWMP2S7UZ817IK971010007099126ENKZP311DEYDCXFTS2S27HTFS3W1HBM"


//http://mobileapi.fsrar.ru/api/mark?DataMatrix=228-06774519126220517312268008442&PDF417=22N00002NWMP2S7UZ817IK971010007099126ENKZP311DEYDCXFTS2S27HTFS3W1HBM

//http://mobileapi.fsrar.ru/api/mark?DataMatrix=134-102630261408200117803974797066&PDF417=22N0000152419RB0W3K37TX71010005056408N85KJRN91OIC93779VG73KV0XM30J5K

