import QtQuick 2.11
import QtQuick.Controls 2.4
import QtMultimedia 5.12
import QZXing 2.3
import FrameAnalyzer 1.0
import BarcodeDecodeFilter 1.0
import QtGraphicalEffects 1.0
import "controls" as Controls

Page {
//    anchors.fill: parent
    title: qsTr("Page 1")

    Connections {
        target: scanManager
        onDataMatrixChanged: {
            snackBar.open("Отсканируйте второй код марки")
        }

        onPdf417Changed: {
            snackBar.open("Отсканируйте второй код марки")
        }

        onQrCodeChanged: {
            snackBar.open("Это продуктовый чек. Отсканируйте штрих-код акцизной марки")
        }

        onScanError: {
            snackBar.open("Это не штрих-код акцизной марки")
        }

        onExciseReady: {
            qsTr()
            snackBar.open("", scanManager.pdf417)
        }
    }

    Camera {
        id: camera

        focus {
            focusMode: CameraFocus.FocusContinuous
            focusPointMode: CameraFocus.FocusPointAuto
        }

//        imageProcessing.whiteBalanceMode: CameraImageProcessing.WhiteBalanceAuto

//        exposure {
//            exposureCompensation: -1.0
//            exposureMode:  Camera.ExposureAuto
//        }

//        flash.mode: Camera.FlashOff
//        captureMode: Camera.CaptureStillImage

//        focus {
//            focusMode: Camera.FocusMacro
//            focusPointMode: Camera.FocusPointCenter
////            customFocusPoint: Qt.point(0.2, 0.2) // Focus relative to top-left corner
//        }

//        onLockStatusChanged: {
//            console.log("STATUS", lockStatus);
//            if (lockStatus == Camera.Unlocked) {
//                searchAndLock();
//            }
//        }

//        Component.onCompleted: {
//            searchAndLock();
//            console.log("FOCUS_AUTO", camera.focus.isFocusModeSupported(Camera.FocusAuto));
//            console.log("FOCUS_AREA_FOCUSED", camera.focus.isFocusModeSupported(Camera.FocusAreaFocused));
//            analyzer.source = camera
//        }
    }

    VideoOutput {
        id: viewfinder
        source: camera
        fillMode: VideoOutput.PreserveAspectCrop
        autoOrientation: true
        anchors.fill: parent
        filters: isCameraReady(camera) ?
                     [ zxingFilter ] : []
        Rectangle {
            id: captureZone
            border {
                width: 2
                color: "#7FFF0000"
            }
            color: "transparent"
            width: parent.height * 0.5
            height: parent.height * 0.5
            anchors.centerIn: parent
        }

//        focus: visible
//        Repeater {
//            model: camera.focus.focusZones

//        }

        Item {
            anchors.fill: parent
            Rectangle {
                id: back
                color: "#7F000000"
                anchors.fill: parent
                visible: false
            }

            Rectangle {
                id: mask
                color: "#FF000000"
                width: parent.height
                height: parent.height
                anchors.centerIn: parent
                visible: false

            }

            OpacityMask {
                invert: true
                anchors.fill: parent
                source: back
                maskSource: ShaderEffectSource {
                    anchors.fill: parent
                    anchors.centerIn: parent
                    sourceItem: mask

                    sourceRect: {
                        return Qt.rect(-(viewfinder.x + viewfinder.width - (viewfinder.height - viewfinder.width)) * 0.5,
                                       -(viewfinder.y + viewfinder.height) * 0.5,
                                       viewfinder.width * 2,
                                       viewfinder.height * 2)
                    }
                    visible: false
                }
            }
        }


//        Rectangle {
//            id: rect
//            border {
//              width: 2
//              color: "green"
////              color: status == Camera.FocusAreaFocused ? "green" : "white"
//            }
////                color: "green"
//            smooth: true
////                color: "transparent"

//            // Map from the relative, normalized frame coordinates
//            //              property variant mappedRect:
//            //                  viewfinder.mapNormalizedRectToItem(area);

//            //              x: mappedRect.x
//            //              y: mappedRect.y
//            width: parent.width * 0.5
//            height: parent.width * 0.5
//            anchors.centerIn: parent
//        }
    }

    QZXingFilter {
        id: zxingFilter

        captureRect: {
            viewfinder.contentRect
            viewfinder.sourceRect
            return viewfinder.mapRectToSource(viewfinder.mapNormalizedPointToItem(
                                                  Qt.rect(0.25, 0.25, 0.5, 0.5)))
        }
        decoder {
            enabledDecoders: QZXing.DecoderFormat_DATA_MATRIX | QZXing.DecoderFormat_PDF_417 | QZXing.DecoderFormat_QR_CODE
            onTagFound: {
                console.log(tag + " | " + decoder.foundedFormat() + " | " + decoder.charSet());
                scanManager.addTag(tag, decoder.foundedFormat(), decoder.charSet());
            }
            tryHarder: false
        }

        property int framesDecoded: 0
        property real timePerFrameDecode: 0

        onDecodingFinished:
        {
           timePerFrameDecode = (decodeTime + framesDecoded * timePerFrameDecode) / (framesDecoded + 1);
           framesDecoded++;
           console.log("frame finished: " + succeeded, decodeTime, timePerFrameDecode, framesDecoded);
        }
    }

    FrameAnalyzer {
        id: analyzer
//        source: camera

//        onTagFound {

//        }
    }

    Controls.SnackBar {
        id: snackBar
        duration: 5000

        onClicked: {
            snackBar.close()
        }
    }

    function isDecodeReady(camera) {
        return camera.focus.isFocusModeSupported(
                    CameraFocus.FocusContinuous) &&
                camera.focus.isFocusPointModeSupported(
                    CameraFocus.FocusPointAuto);
    }

    function isCameraReady(camera) {
        return camera.availability === Camera.Available;
    }
}
