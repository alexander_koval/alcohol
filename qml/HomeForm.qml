import QtQuick 2.11
import QtQuick.Controls 2.4

Page {
//    anchors.fill: parent
    title: qsTr("Home")

    Label {
        text: qsTr("You are on the home page.")
        anchors.centerIn: parent
    }
}
