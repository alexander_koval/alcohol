#include "NetworkRequest.h"
#include "NetworkAccessManager.h"
#include <QNetworkRequest>
#include <QSslError>
#include <QList>
#include <QMap>

namespace {
static const QMap<QNetworkReply::NetworkError, QString> networkErrors {
    { QNetworkReply::ConnectionRefusedError, "The remote server refused the connection (the server is not accepting requests)." },

    { QNetworkReply::UnknownNetworkError, "An unknown error related to the server response was detected." }
};
}

NetworkRequest::NetworkRequest(NetworkAccessManagerPtr manager, const QUrl& url)
    : Base()
    , m_manager(manager)
    , m_url(url)
    , m_isBusy(false) {

}

NetworkRequest::~NetworkRequest()
{

}

void NetworkRequest::execute()
{
    if (m_isBusy) { return; }
    if (!m_manager->isNetworkAccessible()) {
        emit error("Network not accesible");
        return;
    }
    m_isBusy = true;
    emit isBusyChanged();
    QNetworkRequest request(m_url);
    m_reply.reset(m_manager->get(request));
    if (m_reply != nullptr) {
        connect(m_reply.get(), &QNetworkReply::finished,
                this, &NetworkRequest::onReplyDelegate);
        connect(m_reply.get(), &QNetworkReply::sslErrors, this,
                &NetworkRequest::onSslErrors);
    }
}

bool NetworkRequest::isBusy() const
{
    return m_isBusy;
}

void NetworkRequest::setUrl(const QUrl &url)
{
    if (url != m_url) {
        m_url = url;
        emit urlChanged();
    }
}

const QUrl &NetworkRequest::url() const
{
    return m_url;
}

void NetworkRequest::onReplyDelegate()
{
    m_isBusy = false;
    emit isBusyChanged();
    if (m_reply == nullptr) {
        emit error("Unexpected error - reply is null");
        return;
    }
    disconnect(m_reply.get(), &QNetworkReply::finished,
               this, &NetworkRequest::onReplyDelegate);
    disconnect(m_reply.get(), &QNetworkReply::sslErrors,
               this, &NetworkRequest::onSslErrors);
    int statusCode = m_reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    const QByteArray& responseBody = m_reply->readAll();
    QNetworkReply::NetworkError replyStatus = m_reply->error();
    if (replyStatus != QNetworkReply::NoError) {
        emit error("ERROR");
    }
    emit requestComplete(statusCode, responseBody);
}

void NetworkRequest::onSslErrors(const QList<QSslError>& errors)
{
    QString sslError;
    for (const QSslError& error : errors) {
        sslError += error.errorString() + "\n";
    }
    emit error(sslError);
}
