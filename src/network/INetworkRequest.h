#ifndef INETWORKREQUEST_H
#define INETWORKREQUEST_H

class QUrl;
class INetworkRequest {
public:
    virtual ~INetworkRequest() = default;
    virtual void execute() = 0;
    virtual bool isBusy() const = 0;
    virtual void setUrl(const QUrl& url) = 0;
    virtual const QUrl& url() const = 0;
};

#endif // INETWORKREQUEST_H
