#ifndef NETWORKREQUEST_H
#define NETWORKREQUEST_H

#include <QObject>
#include <QUrl>
#include "netfwd.h"
#include "INetworkRequest.h"

class QSslError;
class QNetworkReply;
template <typename T> class QList;
class INetworkAccessManager;
class NetworkRequest : public QObject, public INetworkRequest
{
    Q_OBJECT
    using Base = QObject;
public:
    explicit NetworkRequest(NetworkAccessManagerPtr manager, const QUrl& url);

    explicit NetworkRequest(QObject *parent = nullptr) = delete;

    ~NetworkRequest() override;

    void execute() override;

    bool isBusy() const override;

    void setUrl(const QUrl& url) override;

    const QUrl& url() const override;
signals:
    void error(const QString& message);
    void isBusyChanged();
    void requestComplete(int statusCode, const QByteArray& body);
    void urlChanged();

public slots:
    void onReplyDelegate();
    void onSslErrors(const QList<QSslError>& errors);

private:
    NetworkAccessManagerPtr m_manager;
    QNetworkReplyUPtr m_reply;
    QUrl m_url;
    bool m_isBusy;
};

#endif // NETWORKREQUEST_H
