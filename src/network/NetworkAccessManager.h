#ifndef NETWORKACCESSMANAGER_H
#define NETWORKACCESSMANAGER_H

#include <QObject>
#include <memory>
#include "INetworkAccessManager.h"

class NetworkAccessManager : public QObject, public INetworkAccessManager
{
    Q_OBJECT
public:
    explicit NetworkAccessManager(QObject *parent = nullptr);

    ~NetworkAccessManager() override;

    QNetworkReply* get(const QNetworkRequest&) override;

    bool isNetworkAccessible() const override;

    Q_INVOKABLE bool request();
private:
    std::unique_ptr<QNetworkAccessManager> m_manager;

signals:

public slots:
};

#endif // NETWORKACCESSMANAGER_H
