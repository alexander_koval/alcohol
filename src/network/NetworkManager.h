#ifndef NETWORKMANAGER_H
#define NETWORKMANAGER_H

#include <QObject>
#include <memory>
#include <vector>

class QUrl;
class INetworkRequest;
using NetworkRequestPtr = std::shared_ptr<INetworkRequest>;

class NetworkManager : public QObject
{
    Q_OBJECT
public:
    explicit NetworkManager(QObject *parent = nullptr);

    Q_INVOKABLE void request(const QUrl& url);
signals:

public slots:

private:
    std::vector<NetworkRequestPtr> m_requests;

};

#endif // NETWORKMANAGER_H
