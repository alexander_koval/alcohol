#include "NetworkAccessManager.h"
#include "NetworkRequest.h"

NetworkAccessManager::NetworkAccessManager(QObject *parent)
    : QObject(parent)
    , m_manager(std::make_unique<QNetworkAccessManager>()) {

}

NetworkAccessManager::~NetworkAccessManager() {

}

QNetworkReply *NetworkAccessManager::get(const QNetworkRequest& request) {
    return m_manager->get(request);
}

bool NetworkAccessManager::isNetworkAccessible() const {
    return m_manager->networkAccessible() == QNetworkAccessManager::Accessible;
}

bool NetworkAccessManager::request() {


}
