#ifndef NETFWD_H
#define NETFWD_H

#include <memory>

class INetworkRequest;
class NetworkRequest;
using NetworkRequestPtr = std::shared_ptr<INetworkRequest>;

class INetworkAccessManager;
class NetworkAccessManager;
using NetworkAccessManagerPtr = std::shared_ptr<INetworkAccessManager>;

class QNetworkReply;
using QNetworkReplyPtr = std::shared_ptr<QNetworkReply>;
using QNetworkReplyUPtr = std::unique_ptr<QNetworkReply>;

#endif // NETFWD_H
