#ifndef APPCONTEXT_H
#define APPCONTEXT_H

#include <QObject>
#include <QQmlContext>
#include "models/modfwd.h"
#include "network/netfwd.h"


class AppContext : public QObject {
Q_OBJECT
using Base = QObject;
public:
    AppContext(QObject* parent = nullptr);

    void addContextProperty(QQmlContext* context);

private:
    DataManagerPtr m_dataManager;
    ScanManagerPtr m_scanManager;
    NetworkAccessManagerPtr m_networkManager;
};

extern AppContext& context();

extern bool isContextValid();

#endif // APPCONTEXT_H
