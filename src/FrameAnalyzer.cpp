#include "FrameAnalyzer.h"
#include <QVideoFrame>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>

namespace {
//    static const qint64 timeout = 1000;
}

FrameDecoder::FrameDecoder(QVideoFrame &&frame)
    : Base()
    , QRunnable()
    , m_frame(std::move(frame)) {

}

FrameDecoder::~FrameDecoder() {

}

void FrameDecoder::run() {
//        qDebug() << "DECODE";
        if (m_frame.map(QAbstractVideoBuffer::ReadOnly)) {
            QVideoFrame::PixelFormat format = m_frame.pixelFormat();
            if (format == QVideoFrame::Format_YUV420P ||
                    format == QVideoFrame::Format_NV12 ||
                    format == QVideoFrame::Format_NV21) {
                QImage image(m_frame.bits(), m_frame.width(), m_frame.height(), m_frame.bytesPerLine(), QImage::Format_Grayscale8);
                this->decodeImage(image);
            } else if (format == QVideoFrame::Format_BGR32) {
                QImage tmp(m_frame.bits(), m_frame.width(), m_frame.height(), m_frame.bytesPerLine(), QImage::Format_ARGB32);
                QImage image(tmp.convertToFormat(QImage::Format_Grayscale8));
                this->decodeImage(image);
            }
            m_frame.unmap();
        }
}

FrameAnalyzer::FrameAnalyzer()
    : Base()
    , m_source()
    , m_probe()
    , m_status(QCamera::LockStatus::Unlocked)
    , m_reason(QCamera::LockChangeReason::LockLost) {

}

QObject *FrameAnalyzer::source() const {
    return m_source;
}

void FrameAnalyzer::setSource(QObject *source) {
    m_source = source;
    QCamera *camera = qvariant_cast<QCamera*>(source->property("mediaObject"));
    connect(camera, QOverload<QCamera::LockStatus, QCamera::LockChangeReason>::of(&QCamera::lockStatusChanged),
            this, &FrameAnalyzer::onLockStatusChanges);
    if (m_probe.setSource(camera)) {
        connect(&m_probe, &QVideoProbe::videoFrameProbed, this, &FrameAnalyzer::processFrame);
        camera->searchAndLock();
    } else {
        qDebug() << "Couln't set camera to C++ backend";
    }
}

void FrameAnalyzer::processFrame(QVideoFrame frame) {
    if (!frame.isValid()) return;
    int maxThreads = QThreadPool::globalInstance()->maxThreadCount();
    int currTreads = QThreadPool::globalInstance()->activeThreadCount();
//    qDebug() << currTreads << " " << maxThreads;
    if (m_status != QCamera::LockStatus::Locked &&
            m_reason != QCamera::LockChangeReason::LockAcquired &&
            currTreads < maxThreads) {
//        qDebug() << "FRAME PROCESSING " << frame.isValid();
        FrameDecoder* decoder = new FrameDecoder(std::move(frame));
//        connect(decoder, &QZXing::tagFound, this, &FrameAnalyzer::onTagFound);
        connect(decoder, QOverload<const QString&, const QString&, const QString&, const QRectF&>::of(&QZXing::tagFoundAdvanced),
                this, &FrameAnalyzer::onTagFoundAdvanced);
        connect(decoder, &QZXing::decodingFinished, this, &FrameAnalyzer::onDecodingFinished);
//        connect(decoder, static_cast<void (QZXing::*) (const QString&, const QString&, const QString&, const QRectF&)>(&QZXing::tagFoundAdvanced),
//                this, &FrameAnalyzer::onTagFoundAdvanced);
//        connect(decoder, SIGNAL(tagFoundAdvanced(const QString&, const QString&, const QString&, const QRect&)),
//                this, SLOT(onTagFoundAdvanced(const QString&, const QString&, const QString&, const QRect&)));
        QThreadPool::globalInstance()->start(decoder);
//        m_timer.restart();
    }

}

void FrameAnalyzer::onLockStatusChanges(QCamera::LockStatus status, QCamera::LockChangeReason reason) {
    m_status = status;
    m_reason = reason;
}

void FrameAnalyzer::onTagFound(const QString &tag) {
    Q_UNUSED(tag);
//    qWarning() << tag;
}

void FrameAnalyzer::onTagFoundAdvanced(const QString& tag, const QString &format, const QString &charSet, const QRectF &rect) {
    Q_UNUSED(rect);
    qWarning() << tag << " " << format << " " << charSet;
}

void FrameAnalyzer::onDecodingFinished(bool success) {
    qWarning() << "RESULT: " << success;
    if (!success) {
        QCamera *camera = qvariant_cast<QCamera*>(m_source->property("mediaObject"));
        camera->searchAndLock();
    }
}
