#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "AppContext.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    AppContext appContext;
    QQmlApplicationEngine engine;
    QQmlContext* context = engine.rootContext();
    context->setContextProperty("app", &appContext);
    appContext.addContextProperty(context);

    engine.load(QUrl(QStringLiteral("qrc:qml/main.qml")));

    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
