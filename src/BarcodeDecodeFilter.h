#ifndef BARCODEDECODEFILTER_H
#define BARCODEDECODEFILTER_H

#include <QAbstractVideoFilter>

class BarcodeDecodeFilter : public QAbstractVideoFilter {
    Q_OBJECT
    Q_PROPERTY(QObject* source READ source WRITE setSource)
public:
    BarcodeDecodeFilter();

    QVideoFilterRunnable *createFilterRunnable() override;

    QObject *source() const;

    void setSource(QObject *source);

signals:
    void finished(QObject* result);

public slots:
    void onTagFoundAdvanced(const QString& tag, const QString &format, const QString &charSet, const QRectF &rect);

    void onDecodingFinished(bool success);

    void onError(const QString& error);

private:
    QObject* m_source;
};


#endif // BARCODEDECODEFILTER_H
