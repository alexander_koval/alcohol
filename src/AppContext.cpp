#include "AppContext.h"
#include <QZXing.h>
#include "statusbar.h"
#include "FrameAnalyzer.h"
#include "BarcodeDecodeFilter.h"
#include "models/DataManager.h"
#include "models/ScanManager.h"
#include "network/NetworkAccessManager.h"
#include "QException"
#include "Factory.h"

namespace {
    static AppContext* s_context = nullptr;

    class WrongContextException : public QException {
    public:
        void raise() const override { throw *this; }
        WrongContextException* clone() const override { return new WrongContextException(*this); }
    };
}

AppContext::AppContext(QObject *parent)
    : Base(parent)
    , m_dataManager(std::make_shared<DataManager>())
    , m_scanManager(std::make_shared<ScanManager>())
    , m_networkManager(std::make_shared<NetworkAccessManager>())
{
    QZXing::registerQMLTypes();
    qmlRegisterType<FrameAnalyzer>("FrameAnalyzer", 1, 0, "FrameAnalyzer");
    qmlRegisterType<BarcodeDecodeFilter>("BarcodeDecodeFilter", 1, 0, "BarcodeDecodeFilter");
    qmlRegisterType<StatusBar>("StatusBar", 0, 1, "StatusBar");
}

void AppContext::addContextProperty(QQmlContext *context) {
    context->setContextProperty("dataManager", m_dataManager.get());
    context->setContextProperty("scanManager", m_scanManager.get());
    context->setContextProperty("networkManager", static_cast<NetworkAccessManager*>(
                                    m_networkManager.get()));
    s_context = this;
}

AppContext& context() {
    if (!s_context) {
        qFatal("Applicatoin context is empty");
    }
    return *s_context;
}

bool isContextValid() {
    return static_cast<bool>(s_context);
}
