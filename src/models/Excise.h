#ifndef EXCISE_H
#define EXCISE_H

#include <QObject>

class Excise : public QObject
{
    Q_OBJECT
public:
    explicit Excise(QObject* parent = nullptr);
};

#endif // EXCISE_H
