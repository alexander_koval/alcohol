#include "ScanManager.h"
#include "QZXing.h"
#include <QDebug>

ScanManager::ScanManager(QObject *parent) : QObject(parent)
{

}

const QString &ScanManager::pdf417() const
{
    return m_pdf417;
}

void ScanManager::setPDF417(const QString &value)
{
    if (value != m_pdf417) {
        m_pdf417 = value;
        emit pdf417Changed(value);
        if (isAllCollected())
            emit exciseReady();
    }
}

const QString &ScanManager::dataMatrix() const
{
    return m_dataMatrix;
}

void ScanManager::setDataMatrix(const QString &value)
{
    if (value != m_dataMatrix) {
        m_dataMatrix = value;
        emit dataMatrixChanged(value);
        if (isAllCollected())
            emit exciseReady();
    }
}

const QString &ScanManager::qrCode() const
{
    return m_qrCode;
}

void ScanManager::setQRCode(const QString &value)
{
    if (value != m_qrCode) {
        m_qrCode = value;
        emit qrCodeChanged(value);
    }
}

bool ScanManager::isAllCollected()
{
    return !m_pdf417.isEmpty() && !m_dataMatrix.isEmpty();
}

void ScanManager::addTag(const QString &tag, const QString &format, const QString &charSet)
{
    Q_UNUSED(charSet);
    if (format == QZXing::decoderFormatToString(QZXing::DecoderFormat::DecoderFormat_PDF_417)) {
        setPDF417(tag);
    } else if (format == QZXing::decoderFormatToString(QZXing::DecoderFormat::DecoderFormat_DATA_MATRIX)) {
        setDataMatrix(tag);
    } else if (format == QZXing::decoderFormatToString(QZXing::DecoderFormat::DecoderFormat_QR_CODE)) {
        setQRCode(tag);
    } else {
        emit scanError();
    }
}
