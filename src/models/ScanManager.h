#ifndef SCANMANAGER_H
#define SCANMANAGER_H

#include <QObject>

class ScanManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString m_pdf417 READ pdf417 WRITE setPDF417 NOTIFY pdf417Changed FINAL)
    Q_PROPERTY(QString m_dataMatrix READ dataMatrix WRITE setDataMatrix NOTIFY dataMatrixChanged FINAL)
    Q_PROPERTY(QString m_qrCore READ qrCode WRITE setQRCode NOTIFY qrCodeChanged FINAL)

public:
    explicit ScanManager(QObject *parent = nullptr);

    const QString& pdf417() const;

    void setPDF417(const QString& value);

    const QString& dataMatrix() const;

    void setDataMatrix(const QString& value);

    const QString& qrCode() const;

    void setQRCode(const QString& value);

    Q_INVOKABLE bool isAllCollected();

    Q_INVOKABLE void addTag(const QString& tag, const QString& format, const QString& charSet);
signals:
    void pdf417Changed(const QString& pdf417);

    void dataMatrixChanged(const QString& dataMatrix);

    void qrCodeChanged(const QString& qrCode);

    void exciseReady();

    void scanError();

public slots:

private:
    QString m_pdf417;
    QString m_dataMatrix;
    QString m_qrCode;
};

#endif // SCANMANAGER_H
