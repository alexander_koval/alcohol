#ifndef MODELSFWD_H
#define MODELSFWD_H

#include <memory>

class DataManager;
class ScanManager;
class Excise;

using ExcisePtr = std::shared_ptr<Excise>;
using DataManagerPtr = std::shared_ptr<DataManager>;
using ScanManagerPtr = std::shared_ptr<ScanManager>;

#endif // MODELSFWD_H
