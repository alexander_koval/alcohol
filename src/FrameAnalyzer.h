#ifndef FRAMEANALYZER_H
#define FRAMEANALYZER_H

#include <QObject>
#include <QVideoProbe>
#include <QElapsedTimer>
#include <QAbstractVideoSurface>
#include <QRunnable>
#include <QZXing>
#include <QCamera>
#include <QVideoFilterRunnable>

class FrameDecoder : public QZXing, public QRunnable {
Q_OBJECT
using Base = QZXing;
public:
    FrameDecoder(QVideoFrame&& frame);

    ~FrameDecoder() override;

    void run() override;

private:
    QVideoFrame m_frame;
};

class FrameAnalyzer : public QObject
{
Q_OBJECT
Q_PROPERTY(QObject* source READ source WRITE setSource)
using Base = QObject;
public:
    FrameAnalyzer();

    QObject *source() const;

    void setSource(QObject *source);

public slots:
    void processFrame(QVideoFrame frame);

    void onLockStatusChanges(QCamera::LockStatus status, QCamera::LockChangeReason reason);

    void onTagFound(const QString& tag);

    void onTagFoundAdvanced(const QString& tag, const QString &format, const QString &charSet, const QRectF &rect);

    void onDecodingFinished(bool success);

private:
    QObject* m_source;
    QVideoProbe m_probe;
    QCamera::LockStatus m_status;
    QCamera::LockChangeReason m_reason;
};

#endif // FRAMEANALYZER_H
