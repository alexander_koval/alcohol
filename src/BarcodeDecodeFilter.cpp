#include "BarcodeDecodeFilter.h"
#include "BarcodeDecodeRunnable.h"
#include <QCamera>


BarcodeDecodeFilter::BarcodeDecodeFilter()
    : QAbstractVideoFilter()
    , m_source() {

}

QVideoFilterRunnable *BarcodeDecodeFilter::createFilterRunnable() {
    qDebug() << "RUN";
    QCamera *camera = qvariant_cast<QCamera*>(m_source->property("mediaObject"));
    BarcodeDecodeRunnable* filter = new BarcodeDecodeRunnable(camera->lockStatus());
    connect(camera, QOverload<QCamera::LockStatus, QCamera::LockChangeReason>::of(&QCamera::lockStatusChanged),
            filter, &BarcodeDecodeRunnable::onLockStatusChanged);
    connect(filter, QOverload<const QString&, const QString&, const QString&, const QRectF&>::of(&QZXing::tagFoundAdvanced),
            this, &BarcodeDecodeFilter::onTagFoundAdvanced);
    connect(filter, &QZXing::decodingFinished,
            this, &BarcodeDecodeFilter::onDecodingFinished);
    connect(filter, &QZXing::error, this, &BarcodeDecodeFilter::onError);
    return filter;
}

QObject *BarcodeDecodeFilter::source() const {
    return m_source;
}

void BarcodeDecodeFilter::setSource(QObject *source) {
    if (source != m_source) {
        m_source = source;
        QCamera *camera = qvariant_cast<QCamera*>(m_source->property("mediaObject"));
        camera->searchAndLock();
    }
}

void BarcodeDecodeFilter::onTagFoundAdvanced(const QString &tag, const QString &format, const QString &charSet, const QRectF &rect) {
    Q_UNUSED(rect);
    qWarning() << tag << " " << format << " " << charSet;
}

void BarcodeDecodeFilter::onDecodingFinished(bool success) {
    qWarning() << "RESULT " << success;
    if (!success) {
        QCamera *camera = qvariant_cast<QCamera*>(m_source->property("mediaObject"));
        camera->searchAndLock();
    }
}

void BarcodeDecodeFilter::onError(const QString &error) {
    qWarning() << "ERROR " << error;

}
