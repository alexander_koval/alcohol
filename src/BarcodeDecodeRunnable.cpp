#include "BarcodeDecodeRunnable.h"
#include <QtConcurrent/QtConcurrent>

QVideoFrame BarcodeDecodeRunnable::run(QVideoFrame *input, const QVideoSurfaceFormat &surfaceFormat,
                                       QVideoFilterRunnable::RunFlags flags) {
    Q_UNUSED(flags);
    Q_UNUSED(surfaceFormat);
    if (!input->isValid()) return *input;
    if (m_future.isRunning()) return *input;
//    if (m_status != QCamera::LockStatus::Locked &&
//            m_reason != QCamera::LockChangeReason::LockAcquired) return *input;
    if (input->map(QAbstractVideoBuffer::ReadOnly)) {
        qWarning() << "DECODE";
        QVideoFrame::PixelFormat format = input->pixelFormat();
        if (format == QVideoFrame::Format_YUV420P ||
                format == QVideoFrame::Format_NV12 ||
                format == QVideoFrame::Format_NV21) {
            QImage image(input->bits(), input->width(), input->height(), input->bytesPerLine(), QImage::Format_Grayscale8);
            m_future = QtConcurrent::run(std::bind(&QZXing::decodeImage, this, image, -1, -1, false));
        } else if (format == QVideoFrame::Format_BGR32) {
            QImage tmp(input->bits(), input->width(), input->height(), input->bytesPerLine(), QImage::Format_ARGB32);
            QImage image(tmp.convertToFormat(QImage::Format_Grayscale8));
            m_future = QtConcurrent::run(std::bind(&QZXing::decodeImage, this, image, -1, -1, false));
        }
        input->unmap();
    }
    return *input;
}

void BarcodeDecodeRunnable::onLockStatusChanged(QCamera::LockStatus status, QCamera::LockChangeReason reason) {
    qWarning() << status << " " << reason;
    m_status = status;
    m_reason = reason;
}
