#ifndef BARCODEDECODERUNNABLE_H
#define BARCODEDECODERUNNABLE_H

#include <QCamera>
#include <QZXing>



class BarcodeDecodeRunnable : public QZXing, public QVideoFilterRunnable {
public:
    BarcodeDecodeRunnable(QCamera::LockStatus status)
        : m_future()
        , m_status(status)
        , m_reason() {

    }

    QVideoFrame run(QVideoFrame* input, const QVideoSurfaceFormat& surfaceFormat,
                    RunFlags flags) override;

public slots:
    void onLockStatusChanged(QCamera::LockStatus, QCamera::LockChangeReason);

private:
    QFuture<QString> m_future;
    QCamera::LockStatus m_status;
    QCamera::LockChangeReason m_reason;
};


#endif // BARCODEDECODERUNNABLE_H
