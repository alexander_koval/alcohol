TEMPLATE = app
TARGET = alcohol

QT += qml quick core network sql
CONFIG += c++14 qzxing_multimedia

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

DEFINES += QZXING_QML

VPATH += src
INCLUDEPATH += src

HEADERS += \
    src/AppContext.h \
    src/FrameAnalyzer.h \
    src/BarcodeDecodeFilter.h \
    src/BarcodeDecodeRunnable.h \
    src/models/DataManager.h \
    src/models/Excise.h \
    src/models/ScanManager.h \
    src/network/INetworkAccessManager.h \
    src/network/NetworkAccessManager.h \
    src/network/NetworkRequest.h \
    src/network/INetworkRequest.h \
    src/models/modfwd.h \
    src/network/netfwd.h \
    src/network/NetworkManager.h \
    src/Factory.h

SOURCES += \
    src/main.cpp \
    src/FrameAnalyzer.cpp \
    src/BarcodeDecodeFilter.cpp \
    src/BarcodeDecodeRunnable.cpp \
    src/AppContext.cpp \
    src/models/Excise.cpp \
    src/models/DataManager.cpp \
    src/models/ScanManager.cpp \
    src/network/NetworkAccessManager.cpp \
    src/network/NetworkRequest.cpp \
    src/network/NetworkManager.cpp

lupdate_only {    
    SOURCES += qml/*.qml \
            qml/controls/*.qml
}

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

include(modules/qzxing/src/QZXing.pri)
include(modules/statusbar/src/statusbar.pri)

DISTFILES += \
    qml/controls/SnackBar.qml
